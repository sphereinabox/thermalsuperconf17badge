
// thermal camera overlaid on visible camera

#include "cambadge.h"
#include "globals.h"

// states used by this application

#define s_start 0
#define s_run 1
#define s_grab 2
#define s_grabwait 3
#define s_quit 4


#define bufsize (128*96+1)
#define bufstart 8
#define linelength 129

#define ct_bmp 0
#define ct_dir 1
#define ct_avi 2


char camname[12];

void docamname(unsigned int n, unsigned int ct) { // create a formatted filename CAMxxx.BMP or AVI, or dirname CAM000 

    unsigned int i = 0;
    if (ct == ct_dir) {
        camname[i++] = '\\';
    }
    camname[i++] = 'C';
    camname[i++] = 'A';
    camname[i++] = 'M';
    camname[i++] = (n / 1000) + '0';
    camname[i++] = ((n % 1000) / 100) + '0';
    camname[i++] = ((n % 100) / 10) + '0';
    camname[i++] = (n % 10) + '0';

    if (ct == ct_bmp) {
        camname[i++] = '.';
        camname[i++] = 'B';
        camname[i++] = 'M';
        camname[i++] = 'P';
    }
    if (ct == ct_avi) {
        camname[i++] = '.';
        camname[i++] = 'A';
        camname[i++] = 'V';
        camname[i++] = 'I';
    }

    camname[i++] = 0;
}

void processthermalimageoverlay() {
    unsigned int x, y;
    unsigned char * inptr,*outptr;
    int op,np,er,z;

//                    // scroll xstart so I can find camera offset:
//                    xstart = 10 + ((xstart-10+1)%30);
//                    // display current offset over battery
//                    printf(tabx13 taby0 bat "%02d", xstart);
                    
    xstart = 10; // scroll offset for nick                    

    for(y=0;y!=ypixels;y++) {

        for(x=0;x!=xpixels;x++){
            inptr=cambuffer+bufstart+x+y*xpixels;
            op=*inptr;                   

            if(op>0x80) np=0xff;else np=0;
            er=op-np;
            *inptr=np;
            inptr++;//right
            z=(int) *inptr +er*7/16;
            if(z<0) z=0; else if(z>255) z=255;
            *inptr=z;
            inptr+=(xpixels-2); // down & left
            z=(int) *inptr +er*3/16;
             if(z<0) z=0; else if(z>255) z=255;
            *inptr++=z;
            z=(int) *inptr +er*5/16;//down
             if(z<0) z=0; else if(z>255) z=255;
            *inptr++=z;
            z=(int) *inptr +er*1/16; //down & right
             if(z<0) z=0; else if(z>255) z=255;
            *inptr=z;
        }//x
    }///y
                    
       
    if(grideye_valid) {
        // force the min/max to not get too small so that similar temps always
        // appear similar
        // counts are 4 per degree C
                            int16_t min = grideye_min;
                            int16_t max = grideye_max;
    #define MIN_THERMAL_DIFF_COUNTS 4*10
        if (grideye_max - grideye_min < MIN_THERMAL_DIFF_COUNTS)
        {
            // not enough thermal variation
            // force the colors to show similar by adjusting min/max
            int16_t temp = (MIN_THERMAL_DIFF_COUNTS-(grideye_max-grideye_min))/2;
            min -= temp;
            max += temp;
        }

        for(y=0;y!=ypixels;y++) {
            for(x=0;x!=xpixels;x++){
                int grideye_index = x/16 + 8*(7-y/16);
                inptr=cambuffer+bufstart+x+y*xpixels;
                // add some fake thermal data:
    //                                *inptr=(*inptr & 0x80)|( x & 0x7f); // gradient increasing in X
                *inptr=(*inptr & 0x80)|( (((uint32_t)(grideye_raw[grideye_index]-min)*127)/(max-min))); // thermal value
            }//x
        }///y
    }
    
    //monopalette (0,255);
    thermalpalette();
    dispimage(0, 12, xpixels, ypixels, (img_mono | img_revscan), cambuffer+bufstart);

}


char* thermalcam(unsigned int action) {
    static unsigned int state;
    unsigned int x, y, i, d, r, g, b,e,f;
    static unsigned char explock;
    
    static unsigned int camfile = 0, camdir = 0;
    
    switch (action) {
        case act_name: return ("Thermal Cam");
        case act_help: return ("Thermal Camera");

        case act_start:
            // called once when app is selected from menu
            state = s_start;
            cam_enable(cammode_128x96_z1_mono);
            cam_grabenable(camen_grab, bufstart - 1, 0);

            return (0);
    } //switch

    if (action != act_poll) return (0);


    if (butpress & powerbut) state = s_quit; // exit with nonzero value to indicate we want to quit 
    if (butpress & but1) {
        // used to iterate through effects
//        state = s_start;
    }
    
     if (butpress & but2) {
                explock ^= 1;
                cam_setreg(0x13, explock ? 0xe0 : 0xe7);
               
              
            }
    if (butpress & but4) if (led1) led1_on;
        else led1_off;
    
    if (butpress & but5) {
        state = s_grab;
    }

    switch (state) {
        case s_start:
            if(butstate) break; // in case trig held
//            printf(cls top butcol "EXIT  " whi inv "THERMAL" inv butcol "  LIGHT" bot "EFFECT");
            printf(cls top butcol "EXIT  " whi inv "THERMAL" inv butcol "  LIGHT");
            state = s_run;
            for (i = 0; i != cambufsize; cambuffer[i++] = 0);
            
            cam_grabenable(camen_grab, bufstart - 1, 0); // buffer swap for new frame    
            break;

        case s_run:
            if(butstate) break; // in case trig held
            if (!cam_newframe) break;
            printf(tabx9 taby12 butcol);
            if (explock) printf(inv "ExLock" inv);
            else printf("ExLock");
            printf(tabx0 taby11 yel);

//            printf("Thermal");//Floyd=stienberg error diffusion
            printf("Max %3dC  -  Min %3dC", grideye_max/4, grideye_min/4);

            processthermalimageoverlay();

            cam_grabenable(camen_grab, bufstart - 1, 0); // buffer swap for new frame    

            break;
        case s_grab:
            printf(bot whi);
            state = s_start; // default next state
            if (!cardmounted) {
                printf(inv"No Card         " inv del);
                break;
            }

            cam_grabdisable();
            
            if (cam_newframe) {
                processthermalimageoverlay();
            }

            i = FSchdir("\\CAMERA");
            if (i) {
                FSmkdir("CAMERA");
                FSchdir("CAMERA");
            }

            i = 0;
            do { // find first unused filename
                docamname(camfile++, ct_bmp);
                printf(bot "%-21s", camname);
                fptr = FSfopen(camname, FS_READ);
                i = (fptr != NULL);
                if (i) FSfclose(fptr);
            } while (i);

            thermalpalette();
            conv8_24(xpixels * ypixels, bufstart); // RGB565 to 888

            fptr = FSfopen(camname, FS_WRITE);
            FSchdir("\\"); // exit dir for easier tidyup if error

            i = writebmpheader(xpixels, ypixels, 3/*rgb888*/);
            if (i == 0) {
                FSfclose(fptr);
                printf("Err writing header" bot "OK");
                state = s_grabwait;
                break;
            }
            i = FSfwrite(&cambuffer[8], xpixels * ypixels * 3/*rgb888*/, 1, fptr);
            FSfclose(fptr);
            if (i == 0) {
                printf("Err writing image" bot "OK");
                state = s_grabwait;
                break;
            }
            
            break;
        case s_grabwait:
            if (!butpress) break;
            state = s_start;
            cam_grabenable(camen_grab, bufstart - 1, 0);
            break;

        case s_quit:
            cam_grabdisable();
            return ("");

            break;


    }

    return (0);


}


