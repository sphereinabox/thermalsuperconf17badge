# Thermal Camera Badge Hack

Using a panasonic GridEye thermal camera, turn the 2017 Hackaday Superconference badge into a Flir-One ($200+) or Flir C2 ($500) competitor, complete with combined thermal and visible image.

However, the GridEye has a resolution of 8x8 pixels.

![Assembled Badge](hardware/badgephoto.jpg)

Here's what a pair of computer monitors next to a fan looks like:
![Example thermal/visible image](hardware/sampleimage.png)

## Hardware

The [GridEye breakout](http://www.pureengineering.com/projects/grid-eye-breakout) with voltage regulator was made by Pure Engineering, and Pure Engineering implemented the i2c driver in the badge code.

The thermal camera is wired to power and i2c on the 6x2 pin header on the badge.

