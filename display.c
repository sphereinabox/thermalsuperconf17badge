#include "cambadge.h"
#include "globals.h"
#include "font6x8.inc"
#include "monorgb.h" // mono8->rgb565 lookup

// oled & display formatting stuff

void oledcmd(unsigned int d) { // send byte to display, bit 8 set for command, clear for data
    while (SPI1STATbits.SPIBUSY); // in case previous buffered data still being sent
    if (d & 0x100) oled_cd_lo;
    else oled_cd_hi;
    oled_cs_lo;
    SPI1BUF = d;
    while (SPI1STATbits.SPIBUSY);
    oled_cs_hi;
}

void oled_init(void) { //initialise display after powerup. see SSD1351 chip and OLED module datasheet for details
    unsigned int i;
    const unsigned short oledinitdata[] = {
        0x1fd, 0x12,
        0x1fd, 0xb1,
        0x1ae,
        0x1b3, 0xf1,
        0x1ca, 0x7f,
        0x1a2, 0x00,
        0x1a1, 0x00,
#if oled_upscan==1
        0x1a0, 0x35, // colour depth b0 rotate, b1 mirror b2 color orded, b4 v flip, b7,7 colour depth
#else  
        0x1a0, 0x37, // colour depth b0 rotate, b1 mirror b2 color orded, b4 v flip, b7,7 colour depth
#endif
        0x1b5, 0x0C, // GPIO gpio1 backlight power, gpio0 camera powerdown
        0x1ab, 0x01, //function sel
        0x1b4, 0xa0, 0xb5, 0x55, // seg low voltage
        0x1c1, 0xc8, 0x80, 0xc8, // contrast current
        0x1c7, 0x0f, // master current
        0x1b8, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x15, 0x17, 0x19, 0x1b, 0x1d, 0x1f,
        0x21, 0x23, 0x25, 0x27, 0x2a, 0x2d, 0x30, 0x33, 0x36, 0x39, 0x3c, 0x3f, 0x42, 0x45, 0x48, 0x4c, 0x50, 0x54, 0x58, 0x5c, 0x60, 0x64, 0x68, 0x6c,
        0x70, 0x74, 0x78, 0x7d, 0x82, 0x87, 0x8c, 0x91, 0x96, 0x9b, 0xa0, 0xa5, 0xaa, 0xaf, 0xb4,
        0xb1, 0x32, 0xb2, 0xa4, 0x00, 0x00, 0xbb, 0x17, 0xb6, 0x01, 0xbe, 0x05,
        0x1a6, // display mode
        0x115, 0x00, 0x7f, // col
        0x175, 0x00, 0x7f, // row
        0x1af
    };

    oled_rst_hi;
    delayus(oled_reset_time);
    oled_rst_lo;
    delayus(oled_reset_time);
    oled_rst_hi;
    delayus(oled_reset_time);
    
    for (i = 0; i != sizeof (oledinitdata) / 2; oledcmd(oledinitdata[i++]));
     while (SPI1STATbits.SPIBUSY);    oled_cs_hi;
    plotblock(0, 0, dispwidth, dispheight, 0); // clear display memory
    dispx = dispy = 0;
    fgcol = 0xffff;
    bgcol = 0;
   
}

void monopalette(unsigned int min, unsigned int max) {
    unsigned int i, d;
    for (i = 0; i != 256; i++) {
        d = (i - min)*255 / (max - min);
        if (i <= min) d = 0;
        if (i >= max) d = 255;
        palette[i] = rgbto16(d, d, d);
    }
}

void thermalpalette() {
    palette[0] = 0x1068  ;
    palette[1] = 0x1068  ;
    palette[2] = 0x1068  ;
    palette[3] = 0x1068  ;
    palette[4] = 0x1069  ;
    palette[5] = 0x1869  ;
    palette[6] = 0x1869  ;
    palette[7] = 0x1869  ;
    palette[8] = 0x1869  ;
    palette[9] = 0x1869  ;
    palette[10] = 0x1869 ;
    palette[11] = 0x2069 ;
    palette[12] = 0x2069 ;
    palette[13] = 0x2069 ;
    palette[14] = 0x2069 ;
    palette[15] = 0x2069 ;
    palette[16] = 0x2069 ;
    palette[17] = 0x2869 ;
    palette[18] = 0x2869 ;
    palette[19] = 0x2869 ;
    palette[20] = 0x2869 ;
    palette[21] = 0x2869 ;
    palette[22] = 0x3069 ;
    palette[23] = 0x3069 ;
    palette[24] = 0x3069 ;
    palette[25] = 0x3069 ;
    palette[26] = 0x3069 ;
    palette[27] = 0x3069 ;
    palette[28] = 0x3869 ;
    palette[29] = 0x3869 ;
    palette[30] = 0x386A ;
    palette[31] = 0x386A ;
    palette[32] = 0x386A ;
    palette[33] = 0x406A ;
    palette[34] = 0x406A ;
    palette[35] = 0x406A ;
    palette[36] = 0x406A ;
    palette[37] = 0x406A ;
    palette[38] = 0x406A ;
    palette[39] = 0x486A ;
    palette[40] = 0x486A ;
    palette[41] = 0x486A ;
    palette[42] = 0x486A ;
    palette[43] = 0x486A ;
    palette[44] = 0x486A ;
    palette[45] = 0x4869 ;
    palette[46] = 0x4869 ;
    palette[47] = 0x4869 ;
    palette[48] = 0x4869 ;
    palette[49] = 0x4868 ;
    palette[50] = 0x4868 ;
    palette[51] = 0x4868 ;
    palette[52] = 0x4868 ;
    palette[53] = 0x4847 ;
    palette[54] = 0x4847 ;
    palette[55] = 0x4847 ;
    palette[56] = 0x4847 ;
    palette[57] = 0x4846 ;
    palette[58] = 0x4846 ;
    palette[59] = 0x4846 ;
    palette[60] = 0x4846 ;
    palette[61] = 0x4846 ;
    palette[62] = 0x4845 ;
    palette[63] = 0x4845 ;
    palette[64] = 0x4845 ;
    palette[65] = 0x4845 ;
    palette[66] = 0x4844 ;
    palette[67] = 0x4844 ;
    palette[68] = 0x5044 ;
    palette[69] = 0x5044 ;
    palette[70] = 0x5043 ;
    palette[71] = 0x5043 ;
    palette[72] = 0x5043 ;
    palette[73] = 0x5043 ;
    palette[74] = 0x5022 ;
    palette[75] = 0x5022 ;
    palette[76] = 0x5022 ;
    palette[77] = 0x5022 ;
    palette[78] = 0x5022 ;
    palette[79] = 0x5021 ;
    palette[80] = 0x5021 ;
    palette[81] = 0x5021 ;
    palette[82] = 0x5021 ;
    palette[83] = 0x5020 ;
    palette[84] = 0x5020 ;
    palette[85] = 0x5020 ;
    palette[86] = 0x5040 ;
    palette[87] = 0x5040 ;
    palette[88] = 0x5060 ;
    palette[89] = 0x5060 ;
    palette[90] = 0x5080 ;
    palette[91] = 0x5081 ;
    palette[92] = 0x50A1 ;
    palette[93] = 0x50A1 ;
    palette[94] = 0x50C1 ;
    palette[95] = 0x50C1 ;
    palette[96] = 0x50E1 ;
    palette[97] = 0x50E1 ;
    palette[98] = 0x50E1 ;
    palette[99] = 0x5101 ;
    palette[100] = 0x5101;
    palette[101] = 0x5121;
    palette[102] = 0x5121;
    palette[103] = 0x5141;
    palette[104] = 0x5141;
    palette[105] = 0x5161;
    palette[106] = 0x5961;
    palette[107] = 0x5981;
    palette[108] = 0x5981;
    palette[109] = 0x59A1;
    palette[110] = 0x59A2;
    palette[111] = 0x59A2;
    palette[112] = 0x59C2;
    palette[113] = 0x59C2;
    palette[114] = 0x59E2;
    palette[115] = 0x59E2;
    palette[116] = 0x5A02;
    palette[117] = 0x5A02;
    palette[118] = 0x5A22;
    palette[119] = 0x5A22;
    palette[120] = 0x5A42;
    palette[121] = 0x5A42;
    palette[122] = 0x5A42;
    palette[123] = 0x5A62;
    palette[124] = 0x5A62;
    palette[125] = 0x5A82;
    palette[126] = 0x5A82;
    palette[127] = 0x5AA2;
    palette[128] = 0x5278;
    palette[129] = 0x5A58;
    palette[130] = 0x5A58;
    palette[131] = 0x5A58;
    palette[132] = 0x5A58;
    palette[133] = 0x6258;
    palette[134] = 0x6258;
    palette[135] = 0x6258;
    palette[136] = 0x6258;
    palette[137] = 0x6A58;
    palette[138] = 0x6A57;
    palette[139] = 0x6A57;
    palette[140] = 0x7257;
    palette[141] = 0x7257;
    palette[142] = 0x7257;
    palette[143] = 0x7257;
    palette[144] = 0x7A57;
    palette[145] = 0x7A37;
    palette[146] = 0x7A37;
    palette[147] = 0x7A37;
    palette[148] = 0x8237;
    palette[149] = 0x8237;
    palette[150] = 0x8237;
    palette[151] = 0x8A37;
    palette[152] = 0x8A37;
    palette[153] = 0x8A37;
    palette[154] = 0x8A37;
    palette[155] = 0x9237;
    palette[156] = 0x9237;
    palette[157] = 0x9236;
    palette[158] = 0x9236;
    palette[159] = 0x9A36;
    palette[160] = 0x9A36;
    palette[161] = 0x9A16;
    palette[162] = 0x9A16;
    palette[163] = 0xA216;
    palette[164] = 0xA216;
    palette[165] = 0xA216;
    palette[166] = 0xAA16;
    palette[167] = 0xAA16;
    palette[168] = 0xAA16;
    palette[169] = 0xAA16;
    palette[170] = 0xB215;
    palette[171] = 0xB235;
    palette[172] = 0xB235;
    palette[173] = 0xB235;
    palette[174] = 0xB235;
    palette[175] = 0xBA54;
    palette[176] = 0xBA54;
    palette[177] = 0xBA54;
    palette[178] = 0xBA54;
    palette[179] = 0xBA74;
    palette[180] = 0xC273;
    palette[181] = 0xC273;
    palette[182] = 0xC273;
    palette[183] = 0xC273;
    palette[184] = 0xC293;
    palette[185] = 0xCA92;
    palette[186] = 0xCA92;
    palette[187] = 0xCA92;
    palette[188] = 0xCAB2;
    palette[189] = 0xD2B2;
    palette[190] = 0xD2B1;
    palette[191] = 0xD2B1;
    palette[192] = 0xD2D1;
    palette[193] = 0xD2D1;
    palette[194] = 0xDAD1;
    palette[195] = 0xDAD0;
    palette[196] = 0xDAF0;
    palette[197] = 0xDAF0;
    palette[198] = 0xDAF0;
    palette[199] = 0xE2F0;
    palette[200] = 0xE2EF;
    palette[201] = 0xE30F;
    palette[202] = 0xE30F;
    palette[203] = 0xE30F;
    palette[204] = 0xEB0F;
    palette[205] = 0xEB2E;
    palette[206] = 0xEB2E;
    palette[207] = 0xEB2E;
    palette[208] = 0xEB2E;
    palette[209] = 0xF34E;
    palette[210] = 0xF34D;
    palette[211] = 0xF34D;
    palette[212] = 0xF36D;
    palette[213] = 0xF36D;
    palette[214] = 0xF38D;
    palette[215] = 0xF3AD;
    palette[216] = 0xF3AC;
    palette[217] = 0xF3CC;
    palette[218] = 0xF3EC;
    palette[219] = 0xF40C;
    palette[220] = 0xF40C;
    palette[221] = 0xF42C;
    palette[222] = 0xF44C;
    palette[223] = 0xF44B;
    palette[224] = 0xF46B;
    palette[225] = 0xF48B;
    palette[226] = 0xF48B;
    palette[227] = 0xF4AB;
    palette[228] = 0xF4CB;
    palette[229] = 0xF4EA;
    palette[230] = 0xF4EA;
    palette[231] = 0xF50A;
    palette[232] = 0xF52A;
    palette[233] = 0xF52A;
    palette[234] = 0xF54A;
    palette[235] = 0xF569;
    palette[236] = 0xF569;
    palette[237] = 0xF589;
    palette[238] = 0xF5A9;
    palette[239] = 0xF5A9;
    palette[240] = 0xF5C9;
    palette[241] = 0xF5E9;
    palette[242] = 0xF608;
    palette[243] = 0xF608;
    palette[244] = 0xF628;
    palette[245] = 0xF648;
    palette[246] = 0xF648;
    palette[247] = 0xF668;
    palette[248] = 0xF687;
    palette[249] = 0xF687;
    palette[250] = 0xF6A7;
    palette[251] = 0xF6C7;
    palette[252] = 0xF6C7;
    palette[253] = 0xF6E7;
    palette[254] = 0xF706;
    palette[255] = 0xF726;

}

void thermalpalettesingle()
{
    palette[0] = 0x5278;
    palette[1] = 0x5258;
    palette[2] = 0x5A58;
    palette[3] = 0x5A58;
    palette[4] = 0x5A58;
    palette[5] = 0x5A58;
    palette[6] = 0x5A58;
    palette[7] = 0x5A58;
    palette[8] = 0x5A58;
    palette[9] = 0x5A58;
    palette[10] = 0x6258;
    palette[11] = 0x6258;
    palette[12] = 0x6258;
    palette[13] = 0x6258;
    palette[14] = 0x6258;
    palette[15] = 0x6258;
    palette[16] = 0x6258;
    palette[17] = 0x6258;
    palette[18] = 0x6A58;
    palette[19] = 0x6A58;
    palette[20] = 0x6A57;
    palette[21] = 0x6A57;
    palette[22] = 0x6A57;
    palette[23] = 0x6A57;
    palette[24] = 0x6A57;
    palette[25] = 0x6A57;
    palette[26] = 0x7257;
    palette[27] = 0x7257;
    palette[28] = 0x7257;
    palette[29] = 0x7257;
    palette[30] = 0x7257;
    palette[31] = 0x7257;
    palette[32] = 0x7257;
    palette[33] = 0x7A57;
    palette[34] = 0x7A37;
    palette[35] = 0x7A37;
    palette[36] = 0x7A37;
    palette[37] = 0x7A37;
    palette[38] = 0x7A37;
    palette[39] = 0x7A37;
    palette[40] = 0x7A37;
    palette[41] = 0x8237;
    palette[42] = 0x8237;
    palette[43] = 0x8237;
    palette[44] = 0x8237;
    palette[45] = 0x8237;
    palette[46] = 0x8237;
    palette[47] = 0x8237;
    palette[48] = 0x8237;
    palette[49] = 0x8A37;
    palette[50] = 0x8A37;
    palette[51] = 0x8A37;
    palette[52] = 0x8A37;
    palette[53] = 0x8A37;
    palette[54] = 0x8A37;
    palette[55] = 0x8A37;
    palette[56] = 0x9237;
    palette[57] = 0x9237;
    palette[58] = 0x9237;
    palette[59] = 0x9237;
    palette[60] = 0x9236;
    palette[61] = 0x9236;
    palette[62] = 0x9236;
    palette[63] = 0x9236;
    palette[64] = 0x9A36;
    palette[65] = 0x9A36;
    palette[66] = 0x9A36;
    palette[67] = 0x9A36;
    palette[68] = 0x9A16;
    palette[69] = 0x9A16;
    palette[70] = 0x9A16;
    palette[71] = 0x9A16;
    palette[72] = 0xA216;
    palette[73] = 0xA216;
    palette[74] = 0xA216;
    palette[75] = 0xA216;
    palette[76] = 0xA216;
    palette[77] = 0xA216;
    palette[78] = 0xA216;
    palette[79] = 0xA216;
    palette[80] = 0xAA16;
    palette[81] = 0xAA16;
    palette[82] = 0xAA16;
    palette[83] = 0xAA16;
    palette[84] = 0xAA16;
    palette[85] = 0xAA16;
    palette[86] = 0xAA16;
    palette[87] = 0xAA16;
    palette[88] = 0xB215;
    palette[89] = 0xB235;
    palette[90] = 0xB235;
    palette[91] = 0xB235;
    palette[92] = 0xB235;
    palette[93] = 0xB235;
    palette[94] = 0xB235;
    palette[95] = 0xB235;
    palette[96] = 0xB235;
    palette[97] = 0xBA55;
    palette[98] = 0xBA54;
    palette[99] = 0xBA54;
    palette[100] = 0xBA54;
    palette[101] = 0xBA54;
    palette[102] = 0xBA54;
    palette[103] = 0xBA54;
    palette[104] = 0xBA54;
    palette[105] = 0xBA74;
    palette[106] = 0xBA74;
    palette[107] = 0xC274;
    palette[108] = 0xC273;
    palette[109] = 0xC273;
    palette[110] = 0xC273;
    palette[111] = 0xC273;
    palette[112] = 0xC273;
    palette[113] = 0xC273;
    palette[114] = 0xC293;
    palette[115] = 0xC293;
    palette[116] = 0xCA93;
    palette[117] = 0xCA92;
    palette[118] = 0xCA92;
    palette[119] = 0xCA92;
    palette[120] = 0xCA92;
    palette[121] = 0xCA92;
    palette[122] = 0xCAB2;
    palette[123] = 0xCAB2;
    palette[124] = 0xCAB2;
    palette[125] = 0xD2B2;
    palette[126] = 0xD2B2;
    palette[127] = 0xD2B1;
    palette[128] = 0xD2B1;
    palette[129] = 0xD2B1;
    palette[130] = 0xD2B1;
    palette[131] = 0xD2D1;
    palette[132] = 0xD2D1;
    palette[133] = 0xD2D1;
    palette[134] = 0xD2D1;
    palette[135] = 0xDAD1;
    palette[136] = 0xDAD1;
    palette[137] = 0xDAD0;
    palette[138] = 0xDAD0;
    palette[139] = 0xDAF0;
    palette[140] = 0xDAF0;
    palette[141] = 0xDAF0;
    palette[142] = 0xDAF0;
    palette[143] = 0xDAF0;
    palette[144] = 0xE2F0;
    palette[145] = 0xE2F0;
    palette[146] = 0xE2F0;
    palette[147] = 0xE30F;
    palette[148] = 0xE30F;
    palette[149] = 0xE30F;
    palette[150] = 0xE30F;
    palette[151] = 0xE30F;
    palette[152] = 0xE30F;
    palette[153] = 0xEB0F;
    palette[154] = 0xEB0F;
    palette[155] = 0xEB0F;
    palette[156] = 0xEB2E;
    palette[157] = 0xEB2E;
    palette[158] = 0xEB2E;
    palette[159] = 0xEB2E;
    palette[160] = 0xEB2E;
    palette[161] = 0xEB2E;
    palette[162] = 0xEB2E;
    palette[163] = 0xF32E;
    palette[164] = 0xF34E;
    palette[165] = 0xF34E;
    palette[166] = 0xF34D;
    palette[167] = 0xF34D;
    palette[168] = 0xF34D;
    palette[169] = 0xF34D;
    palette[170] = 0xF36D;
    palette[171] = 0xF36D;
    palette[172] = 0xF36D;
    palette[173] = 0xF38D;
    palette[174] = 0xF38D;
    palette[175] = 0xF38D;
    palette[176] = 0xF3AD;
    palette[177] = 0xF3AD;
    palette[178] = 0xF3CC;
    palette[179] = 0xF3CC;
    palette[180] = 0xF3CC;
    palette[181] = 0xF3EC;
    palette[182] = 0xF3EC;
    palette[183] = 0xF3EC;
    palette[184] = 0xF40C;
    palette[185] = 0xF40C;
    palette[186] = 0xF40C;
    palette[187] = 0xF42C;
    palette[188] = 0xF42C;
    palette[189] = 0xF42C;
    palette[190] = 0xF44B;
    palette[191] = 0xF44B;
    palette[192] = 0xF46B;
    palette[193] = 0xF46B;
    palette[194] = 0xF46B;
    palette[195] = 0xF48B;
    palette[196] = 0xF48B;
    palette[197] = 0xF48B;
    palette[198] = 0xF4AB;
    palette[199] = 0xF4AB;
    palette[200] = 0xF4AB;
    palette[201] = 0xF4CB;
    palette[202] = 0xF4CA;
    palette[203] = 0xF4CA;
    palette[204] = 0xF4EA;
    palette[205] = 0xF4EA;
    palette[206] = 0xF4EA;
    palette[207] = 0xF50A;
    palette[208] = 0xF50A;
    palette[209] = 0xF52A;
    palette[210] = 0xF52A;
    palette[211] = 0xF52A;
    palette[212] = 0xF54A;
    palette[213] = 0xF54A;
    palette[214] = 0xF54A;
    palette[215] = 0xF569;
    palette[216] = 0xF569;
    palette[217] = 0xF569;
    palette[218] = 0xF589;
    palette[219] = 0xF589;
    palette[220] = 0xF589;
    palette[221] = 0xF5A9;
    palette[222] = 0xF5A9;
    palette[223] = 0xF5C9;
    palette[224] = 0xF5C9;
    palette[225] = 0xF5C9;
    palette[226] = 0xF5E9;
    palette[227] = 0xF5E8;
    palette[228] = 0xF5E8;
    palette[229] = 0xF608;
    palette[230] = 0xF608;
    palette[231] = 0xF608;
    palette[232] = 0xF628;
    palette[233] = 0xF628;
    palette[234] = 0xF628;
    palette[235] = 0xF648;
    palette[236] = 0xF648;
    palette[237] = 0xF668;
    palette[238] = 0xF668;
    palette[239] = 0xF668;
    palette[240] = 0xF687;
    palette[241] = 0xF687;
    palette[242] = 0xF687;
    palette[243] = 0xF6A7;
    palette[244] = 0xF6A7;
    palette[245] = 0xF6A7;
    palette[246] = 0xF6C7;
    palette[247] = 0xF6C7;
    palette[248] = 0xF6C7;
    palette[249] = 0xF6E7;
    palette[250] = 0xF6E7;
    palette[251] = 0xF6E7;
    palette[252] = 0xF706;
    palette[253] = 0xF706;
    palette[254] = 0xF726;
    palette[255] = 0xF726;    
}

void plotblock(unsigned int xstart, unsigned int ystart, unsigned int xsize, unsigned int ysize, unsigned int col)
{
    unsigned int i;

    
 if ((xstart + xsize > dispwidth) || (ystart + ysize > dispheight) || (xsize == 0) || (ysize == 0)) return; 
    
        oled_cs_lo;
        oled_cd_lo;
        SPI1BUF=0x75; while (SPI1STATbits.SPIBUSY); oled_cd_hi;
        SPI1BUF=xstart; 
        SPI1BUF=xstart + xsize - 1; 
        while (SPI1STATbits.SPIBUSY);
    

#if oled_upscan==1
       oled_cd_lo;      
        SPI1BUF=0x15; 
        i=((dispheight - ystart - 1) - ysize + 1); 
        while (SPI1STATbits.SPIBUSY); 
        oled_cd_hi;
        SPI1BUF=i; 
        SPI1BUF=dispheight - ystart - 1;
        while (SPI1STATbits.SPIBUSY); 

#else
        
        oled_cd_lo;      
        SPI1BUF=0x15; 
       while (SPI1STATbits.SPIBUSY); 
        oled_cd_hi;
        SPI1BUF=ystart; 
        SPI1BUF=ystart + ysize - 1;
        while (SPI1STATbits.SPIBUSY); 

#endif  
    
 
oled_cd_lo;      
        SPI1BUF=0x15c;   //send data
        i = xsize*ysize;
       while (SPI1STATbits.SPIBUSY); 
        oled_cd_hi;
     SPI1CONbits.MODE16 = 1; // 16 bit for ease of accesss - no speed improvement

    do {
        while (SPI1STATbits.SPITBF);
        SPI1BUF = col;
        
    } while(--i);
      while (SPI1STATbits.SPIBUSY); // wait until last byte sent before releasing CS. if we were DMAing, this would be done in DMA complete int
    SPI1CONbits.MODE16 = 0; // back to 8 bit mode
    oled_cs_hi;
}


void mplotblock(unsigned int x, unsigned int y, unsigned int width, unsigned int height, unsigned int colour, unsigned char* imgaddr) {
    // plot block in memory buffer for subsequent display using dispimage, 8bpp only.
    // speeds up displays of lots of pixels.
    
    unsigned int xx, yy, gap;
  
    gap = dispwidth - width;
    imgaddr += (y * dispwidth + x);
    
    if(((x+width)>dispwidth) || ((y+height)>dispheight)) return;
    
    for (yy = 0; yy != height; yy++) {
        for (xx = 0; xx != width; xx++) *imgaddr++ = (unsigned char) colour;
        imgaddr += gap;
    }
   

}

void dispimage(unsigned int xstart, unsigned int ystart, unsigned int xsize, unsigned int ysize, unsigned int format, unsigned char* imgaddr) { // display image or solid colour in various formats. Note assumes format = bytes per pixel
    // for solid, image pointer is solid colour value, called via plotblock macro
    unsigned int i, d, e, y, x, r, g, b, bpp, skip,vdup,vcount;
    unsigned char *imgaddr2;
    bpp = format & 3;
    skip = (format & 0xf0) >> 4;
    vdup=(format & img_vdouble)?2:1;
   
#if oled_upscan==1
    format ^= img_revscan;
#endif
    if ((xstart + xsize > dispwidth) || (ystart + ysize > dispheight) || (xsize == 0) || (ysize == 0)) return;
    
    oledcmd(0x175);
    oledcmd(xstart);
    oledcmd(xstart + xsize - 1); // column address
#if oled_upscan==1
    oledcmd(0x115);
    oledcmd((dispheight - ystart - 1) - ysize*vdup + 1);
    oledcmd(dispheight - ystart - 1); // row address
#else   
    oledcmd(0x115);
    oledcmd(ystart);
    oledcmd(ystart + ysize*vdup - 1); // row address
#endif 

    oledcmd(0x15c); //send data

    i = xsize*ysize;
    if (((unsigned int) imgaddr + i * bpp * (skip + 1)) >= ((unsigned int) &cambuffer + cambufsize)) return;

    oled_cd_hi;
    oled_cs_lo;
    SPI1CONbits.MODE16 = 1; // 16 bit for ease of accesss - no speed improvement

    //contrary to what datasheet implies, it doesn't seem necessary to de-assert CS between bytes, 
    // so we can use buffered mode to avoid gaps between bytes for higher throughput.

    // Code doesn't need to be super-efficient as speed is limited by OLED max SPI clock rate. 
    // as long as we can produce 1 pixel every 1.3uS avaraged over the 8 pixel FIFO size we're maxing out the SPI bus


    for (y = 0; y != ysize; y++) {
        
        for(vcount=0;vcount!=vdup;vcount++) {
            
        if (format & img_revscan) imgaddr2 = imgaddr + (ysize - y - 1) * xsize * bpp *(skip + 1);
        else imgaddr2 = imgaddr + y * xsize * bpp * (skip + 1);

        for (x = 0; x != xsize; x++) {
            switch (bpp) {
                case 0: d = (unsigned int) imgaddr;
                    break;

                case 1: d = palette[*imgaddr2++];
                    imgaddr2 += skip;
                    break;
                case 2:
                    d = *imgaddr2++;
                    d |= (*imgaddr2++) << 8;
                    imgaddr2 += skip * 2;
                    break;

                case 3:
                    b = *imgaddr2++;
                    g = *imgaddr2++;
                    r = *imgaddr2++;
                    d = (r << 8 & 0xf800) | (g << 3 & 0x7C0) | (b >> 3);
                    imgaddr2 += skip * 3;
            } // switch bpp   

            while (SPI1STATbits.SPITBF);
            SPI1BUF = d;
        } // for x
        } // for vcount
    }// for y 


    while (SPI1STATbits.SPIBUSY); // wait until last byte sent before releasing CS. if we were DMAing, this would be done in DMA complete int
    SPI1CONbits.MODE16 = 0; // back to 8 bit mode
    oled_cs_hi;

}

void _mon_putc(char c) // STDIO for printf
{
    dispchar(c);
}

void dispchar(unsigned char c) {// display 1 character, do control characters
    unsigned int x, y, b, m;

    if (dispuart) {
      
        if (dispuart & dispuart_u1) u1txbyte(c);
        else u2txbyte(c); //UART mode
        if(!(dispuart & dispuart_screen) ) return;
    }

    switch (c) { // control characters

        case 2: //0.5s delay
            delayus(500000);
            break;
        case 3: // half space
            dispx += charwidth / 2;
            break;
        case 4: // short backspace
            dispx -=3;
            break;
        case 7:
            x = fgcol;
            fgcol = bgcol;
            bgcol = x;
            break; // invert  

        case 8:// BS 
            if (dispx >= charwidth) dispx -= charwidth;
            break;
        case 10:// crlf 
            dispx = 0;
            dispy += vspace;
            if (dispy >= dispheight) dispy = 0;
            break;
        case 12: // CLS 
            plotblock(0, 0, dispwidth, dispheight, bgcol);
            dispx = dispy = 0;
            break;
        case 13:
            dispx = 0;
            break; // CR
        case 14 : // grey
            fgcol=rgbto16(128,128,128);
            break;

        case 0x80 ... 0x93: // tab x
            dispx = (c & 0x1f) * charwidth;
            break;
        case 0xa0 ... 0xaf: // tab y
            dispy = (c & 0x0f) * vspace;
            break;

        case 0xc0 ... 0xff: // set text primary colour 0b11rgbRGB bg FG
            fgcol = rgbto16((c & 1) ? 255 : 0, (c & 2) ? 255 : 0, (c & 4) ? 255 : 0);
            bgcol = rgbto16((c & 8) ? 255 : 0, (c & 16) ? 255 : 0, (c & 32) ? 255 : 0);
            break;

        case startchar ... (nchars_6x8 + startchar - 1): // displayed characters
            oled_cs_lo;
            oledcmd(0x175);
            oledcmd(dispx);
            oledcmd(dispx + charwidth - 1); // column address
#if oled_upscan==1
            oledcmd(0x115);
            oledcmd((dispy + charheight - 1)^127);
            oledcmd(dispy^127); // row address

#else
            oledcmd(0x115);
            oledcmd(dispy);
            oledcmd(dispy + charheight - 1); // row address
#endif
            oledcmd(0x15c); //send data  
            SPI1CONbits.MODE16 = 1; // 16 bit SPI so 1 transfer per pixel
            oled_cd_hi;
            oled_cs_lo;
            c -= startchar;
            for (y = 0; y != charheight; y++) {
#if oled_upscan==1
                b = FONT6x8[c][7 - y]; //lookup outside loop for speed    
#else
                b = FONT6x8[c][y]; //lookup outside loop for speed  
#endif          
                for (x = 0; x != charwidth; x++) {
                    while (SPI1STATbits.SPITBF);
                    SPI1BUF = (b & 0x80) ? fgcol : bgcol;
                    b <<= 1;
                }
            } //y
            while (SPI1STATbits.SPIBUSY); // wait until last byte sent before releasing CS
            SPI1CONbits.MODE16 = 0;
            oled_cs_hi;
            dispx += charwidth;
            if (dispx >= dispwidth) {
                dispx = 0;
                dispy += vspace;
                if (dispy >= dispheight) dispy = 0;
            }
            break;

    }//switch

  oled_cs_hi;

}
